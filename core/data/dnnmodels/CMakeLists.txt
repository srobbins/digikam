#
# SPDX-FileCopyrightText: 2010-2025 by Gilles Caulier, <caulier dot gilles at gmail dot com>
# SPDX-FileCopyrightText: 2024      by Michael Miller, <michael underscore miller at msn dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
#

install(FILES dnnmodels.conf
        DESTINATION ${KDE_INSTALL_FULL_DATADIR}/digikam/dnnmodels
)
