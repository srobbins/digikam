digiKam 8.6.0 - Release date: 2025-xx-xx

*****************************************************************************************************
NEW FEATURES:

RawEngine:       Update internal Libraw to last 20241203 upstream.
Video:           Update internal QtAVPlayer to last 20241210 upstream (Qt5 only).
Bundles:         Update to last ExifTool version 13.10.
Setup:           Add new GPU hardware support options for decoding video with FFmpeg backend (Qt6 only).
FaceEngine:      Complete rewrite of the Face Classifier and Face Pipeline implmentations.
                 There are significant improvements coming in 8.6.0. The pipelines have
                 been completely rewritten and are 25%-50% faster when using full CPU. The face
                 detector has also been improved to reduce false positives. There is a
                 completely new face classifier using cross-validating KNN and SVM internal
                 classifiers that increases matching accuracy even more.

*****************************************************************************************************
BUGFIXES:

001 ==> 389046 - Face suggestions disappair each time i confirm one.
002 ==> 496379 - No longer possible to have RAW files as sidecars.
003 ==> 496389 - Tool to determin time difference from clock photo show diffent timestamp.
004 ==> 496381 - Possibility to compile MacOS-x86_64 against Qt6?
005 ==> 496395 - Check for New Version.
006 ==> 496378 - No Scanning of Collections/Albums Happen.
007 ==> 496436 - Improvement in Miscellaneous settings.
008 ==> 496340 - Missing Context Menu on edit advanced search.
009 ==> 496427 - After updating from 8.4.0 to 8.5.0 digiKam there is only a white screen on start-up.
010 ==> 496474 - digiKam 8.5 under macOS (Apple Silicon) - problem displaying HEIC of iPhone 16 pro.
011 ==> 496570 - {unique} does not take file extension into account.
012 ==> 496711 - Link to documentation in People View broken.
013 ==> 495722 - Assigning rating via keyboard shortcut doesnt work as expected (toggling vs assigning rating).
014 ==> 496619 - Fedora 41 appimage: symbol lookup error: /usr/lib64/libgnutls.so.30.
015 ==> 496771 - It is no longer possible to convert RAW images to DNG images.
016 ==> 496785 - digiKam appimage does not start with symbol lookup error: undefined symbol: nettle_rsa_oaep_sha384_decrypt.
017 ==> 496868 - The application only starts with a white window.
018 ==> 496867 - digiKam 8.0 does not display a new album folder.
019 ==> 480357 - digiKam 8.2.0 segfaults on importing collection.
020 ==> 477379 - digiKam crashes on start with "failed to create drawable" / "glXCreatePbuffer failed.".
021 ==> 492836 - Appimage Integration failed.
022 ==> 493578 - Impossible to integrate the appimage 8.4.0 and 8.5.0 to the desktop.
023 ==> 492813 - Appimage crashes when trying to integrate into KDE Neon 6.1 (Ubuntu 24.04 based).
024 ==> 497037 - Error message when trying to download version 8.5.0.
025 ==> 497058 - Installed digikam dumps core.
026 ==> 490377 - Failed to update -- see attached screen shot.
027 ==> 490378 - Check for upgrade fails to download. Files ending with .sha256.
028 ==> 470342 - digiKam dont start.
029 ==> 497106 - Rating from images is no longer respected.
030 ==> 472138 - Request for disabling more components at compile time.
031 ==> 496484 - (mov.) video playback distorted colors.
032 ==> 485468 - digiKam crash with no attention when preview webm.
033 ==> 497147 - Launching of v8.5 creates wrong initial screen: a copy of the desktop.
034 ==> 497217 - Expand Batch Queue Manager Resize to include larger preset lengths and revise text.
035 ==> 497264 - digiKam dock icon changes and becomes pixelated after running app.
036 ==> 497446 - Video seek timeline/scrubber is unusable in slideshow mode.
037 ==> 497508 - Data loss: metadata not saved despite enabling in 1st launch wizard.
038 ==> 497640 - digiKam ARM64 pkg requires Rosetta to run the Installer.
039 ==> 497476 - Create Tag from Address Book not functioning in Tags Manager on macOS.
040 ==> 497642 - Edit metadata plugin don't allow you to edit metadata of the selected image from Showfoto.
041 ==> 496691 - Duplicate search has become much slower on the release version of digiKam 8.5.
042 ==> 493988 - digiKam crashes when trying to review some .mov files.
043 ==> 497743 - Option to avoid saving thumbnails.
044 ==> 497775 - Your Flatpak installation is setuid, which is not supported.
045 ==> 497824 - Wrong location displayed for mp4.
046 ==> 497791 - Unwanted face recognition function in preview mode when face management is disabled.
047 ==> 497814 - Add Support for Configurable Output Color Profile When Editing with darktable.
048 ==> 497567 - Feature Request: "No New Images for Deceased Persons".
049 ==> 459499 - The possibility to disable persons.
050 ==> 498005 - Provide shortcuts for videos in the slideshow.
051 ==> 498025 - Excluded directory is still not excluded.
052 ==> 498041 - Debian testing doesn't need libgnutls.so.30 from AppImage.
053 ==> 498053 - The use of XMP metadata in custom renaming patterns does not work as expected.
054 ==>
055 ==>
056 ==>
057 ==>
058 ==>
059 ==>
060 ==>
061 ==>
062 ==>
063 ==>
064 ==>
065 ==>
066 ==>
067 ==>
068 ==>
069 ==>
070 ==>
071 ==>
072 ==>
073 ==>
074 ==>
075 ==>
076 ==>
077 ==>
078 ==>
079 ==>
080 ==>
081 ==>
082 ==>
083 ==>
084 ==>
085 ==>
086 ==>
087 ==>
088 ==>
089 ==>
090 ==>
091 ==>
092 ==>
093 ==>
094 ==>
095 ==>
096 ==>
097 ==>
098 ==>
099 ==>
100 ==>
101 ==>
102 ==>
103 ==>
104 ==>
105 ==>
106 ==>
107 ==>
108 ==>
109 ==>
110 ==>
111 ==>
112 ==>
113 ==>
114 ==>
115 ==>
116 ==>
117 ==>
118 ==>
119 ==>
120 ==>
121 ==>
122 ==>
123 ==>
124 ==>
125 ==>
126 ==>
127 ==>
128 ==>
129 ==>
130 ==>
131 ==>
132 ==>
133 ==>
134 ==>
135 ==>
136 ==>
137 ==>
138 ==>
139 ==>
140 ==>
141 ==>
142 ==>
143 ==>
144 ==>
145 ==>
146 ==>
147 ==>
148 ==>
149 ==>
150 ==>
151 ==>
152 ==>
153 ==>
154 ==>
155 ==>
156 ==>
157 ==>
158 ==>
159 ==>
160 ==>
161 ==>
162 ==>
163 ==>
164 ==>
165 ==>
166 ==>
167 ==>
168 ==>
169 ==>
170 ==>
